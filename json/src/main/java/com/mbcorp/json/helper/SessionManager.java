package com.mbcorp.json.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Minh on 7/22/2016.
 */
public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AndroidHiveLogin";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String EMAIL_LOGGED = "EMAIL_LOGGED";
    private static final String ID_LOGGED = "ID_LOGGED";

    public SessionManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn, String email, String id) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.putString(EMAIL_LOGGED, email);
        editor.putString(ID_LOGGED, id);
        editor.commit();
    }

    public String emailLogged() {
        return pref.getString(EMAIL_LOGGED, "");
    }

    public String idLogged() {
        return pref.getString(ID_LOGGED, "");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}
