package com.mbcorp.json.app;

/**
 * Created by Minh on 7/22/2016.
 */
public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://minibangcorporation.ga/Login.php";

    // Server user register url
    public static String URL_REGISTER = "http://minibangcorporation.ga/Register.php";

    // Server event url
    public static String URL_EVENT = "http://minibangcorporation.ga/Events.php";

    public static String URL_GET_EVENT = "http://minibangcorporation.ga/GetEvents.php";
}
