package com.mbcorp.textfontslib;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Minh on 7/13/2016.
 */
public class Utils {
    public static Typeface customTextView(Context context, int type) {
        switch (type) {
            case 0:
                return setTextView(context, "BreeSerif-Regular.ttf");

            case 1:
                return setTextView(context, "Roboto-Bold.ttf");

            case 2:
                return setTextView(context, "Roboto-Italic.ttf");

            case 3:
                return setTextView(context, "Roboto-Light.ttf");

            case 4:
                return setTextView(context, "Roboto-LightItalic.ttf");

            case 5:
                return setTextView(context, "Roboto-Medium.ttf");

            case 6:
                return setTextView(context, "Roboto-Regular.ttf");

            case 7:
                return setTextView(context, "Roboto-Thin.ttf");

            case 8:
                return setTextView(context, "Roboto-ThinItalic.ttf");

            case 9:
                return setTextView(context, "Aller_Bd.ttf");

            case 10:
                return setTextView(context, "Aller_BdIt.ttf");

            case 11:
                return setTextView(context, "Aller_It.ttf");

            case 12:
                return setTextView(context, "Aller_Lt.ttf");

            case 13:
                return setTextView(context, "Aller_LtIt.ttf");

            case 14:
                return setTextView(context, "Aller_Rg.ttf");

            case 15:
                return setTextView(context, "AllerDisplay.ttf");

            case 16:
                return setTextView(context, "Sansita-Black.otf");

            case 17:
                return setTextView(context, "Sansita-BlackItalic.otf");

            case 18:
                return setTextView(context, "Sansita-Bold.otf");

            case 19:
                return setTextView(context, "Sansita-BoldItalic.otf");

            case 20:
                return setTextView(context, "Sansita-ExtraBold.otf");

            case 21:
                return setTextView(context, "Sansita-ExtraBoldItalic.otf");

            case 22:
                return setTextView(context, "Sansita-Italic.otf");

            case 23:
                return setTextView(context, "Sansita-Regular.otf");

            case 24:
                return setTextView(context, "DroidSerif-Bold.ttf");

            case 25:
                return setTextView(context, "DroidSerif-BoldItalic.ttf");

            case 26:
                return setTextView(context, "DroidSerif-Italic.ttf");

            case 27:
                return setTextView(context, "DroidSerif-Regular.ttf");

            default:
                return Typeface.DEFAULT;
        }
    }

    private static Typeface setTextView(Context context, String type) {
        try {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), type);
            if (tf != null) {
                return tf;
            } else {
                return Typeface.DEFAULT;
            }
        } catch (Exception e) {
            return Typeface.DEFAULT;
        }
    }
}
