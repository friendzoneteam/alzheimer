package com.mbcorp.textfontslib;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Minh on 7/22/2016.
 */
public class EditTextCustom extends EditText {
    private TypedArray typedArray;

    public EditTextCustom(Context context) {
        super(context);
        setTypeface(Utils.customTextView(context, -1));
    }

    public EditTextCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

    public EditTextCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextCustom(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

}
