package com.mbcorp.guidesignlib;

import android.app.ProgressDialog;

/**
 * Created by Minh on 7/22/2016.
 */
public class ProgressDialogType {
    public static void progressDialogNormal (String message, boolean check, ProgressDialog progressDialog) {
        if(check) {
            progressDialog.setMessage(message);
            progressDialog.show();
        }
        else {
            progressDialog.dismiss();
        }
    }
}
