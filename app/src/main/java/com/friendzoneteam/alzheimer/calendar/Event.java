package com.friendzoneteam.alzheimer.calendar;

import android.support.annotation.Nullable;

import com.friendzoneteam.alzheimer.entity.EventObject;

import java.util.ArrayList;

/**
 * Created by Minh on 7/27/2016.
 */
public class Event {

    private int color;
    private long timeInMillis;
    private EventObject event;

    public Event(int color, long timeInMillis) {
        this.color = color;
        this.timeInMillis = timeInMillis;
    }

    public Event(int color, long timeInMillis, EventObject event) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.event = event;
    }

    public int getColor() {
        return color;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public EventObject getEvent() {
        return event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (color != event.color) return false;
        if (timeInMillis != event.timeInMillis) return false;
        if (event != null ? !event.equals(event.event) : event.event != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        result = 31 * result + (event != null ? event.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "color=" + color +
                ", timeInMillis=" + timeInMillis +
                ", data=" + event +
                '}';
    }
}
