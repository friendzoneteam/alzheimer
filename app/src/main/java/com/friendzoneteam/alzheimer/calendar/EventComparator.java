package com.friendzoneteam.alzheimer.calendar;

import com.friendzoneteam.alzheimer.calendar.Event;

import java.util.Comparator;

/**
 * Created by Minh on 7/27/2016.
 */
public class EventComparator implements Comparator<Event> {

    @Override
    public int compare(Event lhs, Event rhs) {
        return lhs.getTimeInMillis() < rhs.getTimeInMillis() ? -1 : lhs.getTimeInMillis() == rhs.getTimeInMillis() ? 0 : 1;
    }
}
