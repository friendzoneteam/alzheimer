package com.friendzoneteam.alzheimer.calendar;

import android.animation.Animator;

/**
 * Created by Minh on 7/27/2016.
 */
public abstract class AnimatorListener implements Animator.AnimatorListener{

    @Override
    public void onAnimationStart(Animator animation) {
    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

}
