package com.friendzoneteam.alzheimer.calendar;

import android.view.animation.Animation;

/**
 * Created by Minh on 7/27/2016.
 */
public abstract class AnimationListener implements Animation.AnimationListener{

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
