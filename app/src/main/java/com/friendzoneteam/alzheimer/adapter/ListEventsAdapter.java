package com.friendzoneteam.alzheimer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.entity.EventObject;

import java.util.ArrayList;

/**
 * Created by Minh on 7/29/2016.
 */
public class ListEventsAdapter extends BaseAdapter {
    private ArrayList<EventObject> event;
    private Context context;

    public ListEventsAdapter(Context context, ArrayList<EventObject> event){
        this.context = context;
        this.event = event;
    }
    @Override
    public int getCount() {
        return event.size();
    }

    @Override
    public Object getItem(int position) {
        return event.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_events, parent, false);

            viewHolder.content = (TextView) convertView.findViewById(R.id.content);
            viewHolder.up = (RelativeLayout) convertView.findViewById(R.id.up);
            viewHolder.down = (RelativeLayout) convertView.findViewById(R.id.down);
            viewHolder.dot = (ImageView) convertView.findViewById(R.id.dot);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.content.setText("[" + event.get(position).getmStart() + "] " + event.get(position).getmTag());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewHolder.dot.setBackground(ContextCompat.getDrawable(context, event.get(position).getmColor()));
        } else {
            viewHolder.dot.setBackgroundDrawable(ContextCompat.getDrawable(context, event.get(position).getmColor()));
        }

        if (event.size() != 1) {
            if (position != 0 && position != event.size() - 1) {
                viewHolder.up.setVisibility(View.VISIBLE);
                viewHolder.down.setVisibility(View.VISIBLE);
            } else if (position == event.size() - 1) {
                viewHolder.up.setVisibility(View.VISIBLE);
                viewHolder.down.setVisibility(View.INVISIBLE);
            } else if (position == 0) {
                viewHolder.down.setVisibility(View.VISIBLE);
                viewHolder.up.setVisibility(View.INVISIBLE);
            }
        }

        return convertView;
    }

    class ViewHolder{
        private TextView content;
        private RelativeLayout up;
        private RelativeLayout down;
        private ImageView dot;
    }
}