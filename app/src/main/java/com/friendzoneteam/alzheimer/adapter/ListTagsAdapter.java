package com.friendzoneteam.alzheimer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.entity.TagsObject;
import com.mbcorp.textfontslib.TextViewCustom;

import java.util.ArrayList;

/**
 * Created by Minh on 7/25/2016.
 */
public class ListTagsAdapter extends BaseAdapter {
    private ArrayList<TagsObject> tags;
    private Context context;

    public ListTagsAdapter(Context context, ArrayList<TagsObject> tags){
        this.context = context;
        this.tags = tags;
    }
    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return tags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_tags_row, parent, false);

            viewHolder.iconTags = (ImageView) convertView.findViewById(R.id.iconTags);
            viewHolder.nameTags = (TextViewCustom) convertView.findViewById(R.id.nameTags);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.iconTags.setImageResource(tags.get(position).getmTagsIcon());
        viewHolder.nameTags.setText(tags.get(position).getmTagsName());

        return convertView;
    }

    class ViewHolder{
        private ImageView iconTags;
        private TextViewCustom nameTags;
    }
}