package com.friendzoneteam.alzheimer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.entity.ColorsObject;
import com.friendzoneteam.alzheimer.entity.TagsObject;
import com.mbcorp.textfontslib.TextViewCustom;

import java.util.ArrayList;

/**
 * Created by MINH-PC on 8/19/2016.
 */
public class ListColorsAdapter extends BaseAdapter {
    private ArrayList<ColorsObject> colors;
    private Context context;

    public ListColorsAdapter(Context context, ArrayList<ColorsObject> colors){
        this.context = context;
        this.colors = colors;
    }
    @Override
    public int getCount() {
        return colors.size();
    }

    @Override
    public Object getItem(int position) {
        return colors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_colors_row, parent, false);

            viewHolder.iconTags = (ImageView) convertView.findViewById(R.id.iconTags);
            viewHolder.nameTags = (TextViewCustom) convertView.findViewById(R.id.nameTags);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.iconTags.setImageResource(colors.get(position).getColor());
        viewHolder.nameTags.setText(colors.get(position).getColorName());

        return convertView;
    }

    class ViewHolder{
        private ImageView iconTags;
        private TextViewCustom nameTags;
    }
}