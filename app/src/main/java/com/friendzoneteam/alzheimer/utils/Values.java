package com.friendzoneteam.alzheimer.utils;

import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.entity.ColorsObject;
import com.friendzoneteam.alzheimer.entity.TagsObject;

import java.util.ArrayList;

/**
 * Created by Minh on 7/27/2016.
 */
public class Values {
    // request code
    public static final int REQUEST_CODE = 0;
    public static final int RESULT_CODE = 1;
    public static final int TAGS = 4;
    public static final int COLORS = 6;

    public static ArrayList<TagsObject> getTags() {
        int tagsIcon[] = new int[TAGS];
        tagsIcon[0] = R.drawable.ic_eating;
        tagsIcon[1] = R.drawable.ic_reading;
        tagsIcon[2] = R.drawable.ic_pills;
        tagsIcon[3] = R.drawable.ic_sleep;

        String tagsName[] = new String[TAGS];
        tagsName[0] = "Eating";
        tagsName[1] = "Reading";
        tagsName[2] = "Pills";
        tagsName[3] = "Sleep";

        ArrayList<TagsObject> tags = new ArrayList<>();
        TagsObject tag;
        for(int i = 0 ; i < TAGS ; i++) {
            tag = new TagsObject();
            tag.setmTagsIcon(tagsIcon[i]);
            tag.setmTagsName(tagsName[i]);
            tags.add(tag);
        }

        return tags;
    }

    public static ArrayList<ColorsObject> getColors() {
        int colors[] = new int[COLORS];
        colors[0] = R.drawable.image_round_apr;
        colors[1] = R.drawable.image_round_dec;
        colors[2] = R.drawable.image_round_feb;
        colors[3] = R.drawable.image_round_jul;
        colors[4] = R.drawable.image_round_jun;
        colors[5] = R.drawable.image_round_blue_light;

        String colorsname[] = new String[COLORS];
        colorsname[0] = "Green";
        colorsname[1] = "Purple";
        colorsname[2] = "Aquamarine";
        colorsname[3] = "Deep Orange";
        colorsname[4] = "Orange";
        colorsname[5] = "Default Color";

        ArrayList<ColorsObject> colorsObjects = new ArrayList<>();
        ColorsObject color;
        for(int i = 0 ; i < COLORS ; i++) {
            color = new ColorsObject();
            color.setColor(colors[i]);
            color.setColorName(colorsname[i]);
            colorsObjects.add(color);
        }

        return colorsObjects;
    }

    // values
    public static final int TIME_CALENDAR_ANIMATION = 200;

    public static String getMOnth(int month) {
        switch (month) {
            case 1:
                return "Jan";

            case 2:
                return "Feb";

            case 3:
                return "Mar";

            case 4:
                return "Apr";

            case 5:
                return "May";

            case 6:
                return "Jun";

            case 7:
                return "Jul";

            case 8:
                return "Aug";

            case 9:
                return "Sep";

            case 10:
                return "Oct";

            case 11:
                return "Nov";

            case 12:
                return "Dec";
        }

        return "";
    }

    public static int getMonth(String month) {
        switch (month) {
            case "Jan":
                return 1;

            case "Feb":
                return 2;

            case "Mar":
                return 3;

            case "Apr":
                return 4;

            case "May":
                return 5;

            case "Jun":
                return 6;

            case "Jul":
                return 7;

            case "Aug":
                return 8;

            case "Sep":
                return 9;

            case "Oct":
                return 10;

            case "Nov":
                return 11;

            case "Dec":
                return 12;
        }

        return 0;
    }
}
