package com.friendzoneteam.alzheimer.utils;

/**
 * Created by Minh on 7/25/2016.
 */
public class ConvertValues {
    public static String convertDate (int year, int month, int day) {
        if(day < 10) {
            return "0" + day + " - " + Values.getMOnth(month) + " - " + year;
        } else {
            return day + " - " + Values.getMOnth(month) + " - " + year;
        }
    }

    public static String convertTime (int hour, int minute) {
        if(hour < 10) {
            if(minute < 10) {
                return "0" + hour + ":" + "0" + minute;
            } else {
                return "0" + hour + ":" + minute;
            }
        } else {
            if(minute < 10) {
                return hour + ":" + "0" + minute;
            } else {
                return hour + ":" + minute;
            }
        }
    }
}
