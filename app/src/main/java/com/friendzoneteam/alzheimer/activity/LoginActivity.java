package com.friendzoneteam.alzheimer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.utils.Utils;
import com.friendzoneteam.alzheimer.utils.Values;
import com.mbcorp.guidesignlib.ProgressDialogType;
import com.mbcorp.json.app.AppConfig;
import com.mbcorp.json.app.AppController;
import com.mbcorp.json.helper.SessionManager;
import com.mbcorp.textfontslib.EditTextCustom;
import com.mbcorp.textfontslib.TextViewCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditTextCustom editEmail;
    private EditTextCustom editPassword;
    private TextViewCustom btnLogin;
    private TextViewCustom btnRegister;
    private CheckBox checkLogin;

    private ProgressDialog progressDialog;
    private SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // check internet connection
        if (!Utils.hasConnection(this)) {
            Toast.makeText(LoginActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        editEmail = (EditTextCustom) findViewById(R.id.email);
        editPassword = (EditTextCustom) findViewById(R.id.password);
        btnLogin = (TextViewCustom) findViewById(R.id.btnLogin);
        btnRegister = (TextViewCustom) findViewById(R.id.btnRegister);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        checkLogin = (CheckBox) findViewById(R.id.checkLogin);

        session = new SessionManager(this);
        if (session.isLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, LoggedActivity.class);
            intent.putExtra("email", session.emailLogged());
            intent.putExtra("id", session.idLogged());
            startActivity(intent);
            finish();
        } else if (session.emailLogged().compareTo("") != 0) {
            editEmail.setText(session.emailLogged());
        }
    }

    private void checkLogin(final String email, final String password) {

        ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_login), true, progressDialog);

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_login), false, progressDialog);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if (!error) {
                        String id = jsonObject.getString("id");

                        if(checkLogin.isChecked()) {
                            session.setLogin(true, email, id);
                        }

                        Intent intent = new Intent(LoginActivity.this, LoggedActivity.class);
                        intent.putExtra("email", email);
                        intent.putExtra("id", id);
                        startActivity(intent);
                        finish();
                    } else {
                        // wrong email or password
                        String errorMsg = jsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // server do not return value
                    Toast.makeText(LoginActivity.this, "" + e.toString().substring(24), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // server die
                if (error == null) {
                    Toast.makeText(LoginActivity.this, getString(R.string.server_die), Toast.LENGTH_SHORT).show();
                } else {
                    parseVolleyError(error);
                }
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_login), false, progressDialog);
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                JSONObject json = new JSONObject();
                try {
                    json.put("email", email);
                    json.put("password", password);
                } catch (JSONException e) {
                    Toast.makeText(LoginActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }

                // encode json with base64
                try {
                    byte[] data = String.valueOf(json).getBytes("UTF-8");
                    params.put("json", Base64.encodeToString(data, Base64.DEFAULT));
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(LoginActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);
            String message = jsonMessage.getString("message");
            Toast.makeText(getApplicationContext(), error.getMessage() + "\n" + message, Toast.LENGTH_LONG).show();
        } catch (JSONException | UnsupportedEncodingException ignored) {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (!Utils.hasConnection(this)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                } else {
                    String email = editEmail.getText().toString().trim();
                    String password = editPassword.getText().toString().trim();

                    if (!email.isEmpty() && !password.isEmpty()) {
                        checkLogin(email, password);
                    } else {
                        Toast.makeText(this, getString(R.string.login_infor), Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case R.id.btnRegister:
                if (!Utils.hasConnection(this)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                } else {
                    startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), Values.REQUEST_CODE);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    if(requestCode == Values.REQUEST_CODE && resultCode == Values.RESULT_CODE) {
            editEmail.setText(data.getStringExtra("email"));
        }
    }
}