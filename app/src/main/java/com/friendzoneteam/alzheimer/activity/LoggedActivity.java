package com.friendzoneteam.alzheimer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.adapter.ListEventsAdapter;
import com.friendzoneteam.alzheimer.calendar.CompactCalendarView;
import com.friendzoneteam.alzheimer.calendar.Event;
import com.friendzoneteam.alzheimer.entity.EventObject;
import com.friendzoneteam.alzheimer.utils.ConvertValues;
import com.friendzoneteam.alzheimer.utils.Utils;
import com.friendzoneteam.alzheimer.utils.Values;
import com.mbcorp.guidesignlib.ProgressDialogType;
import com.mbcorp.json.app.AppConfig;
import com.mbcorp.json.app.AppController;
import com.mbcorp.json.helper.SessionManager;
import com.mbcorp.textfontslib.TextViewCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class LoggedActivity extends AppCompatActivity implements View.OnClickListener {
    private CompactCalendarView compactCalendarView;
    private RelativeLayout btnArrow;
    private TextViewCustom txtCalendar;
    private ImageView imageArrow, elevation;
    private View lineUp, lineDown;

    private FloatingActionButton fab;

    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("dd - MMM - yyyy", Locale.getDefault());
    private SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.getDefault());
    private boolean checkArrow = false;
    private String month;
    private int color[];
    private int currentColor;
    private String ID;
    private String email;

    private SessionManager session;

    private TransitionDrawable crossfader;
    private ColorDrawable colorDrawables[];

    private ListView listEvents;
    private ListEventsAdapter adapter;
    private ArrayList<EventObject> events;
    private EventObject event;
    private int year, monthEvents, day, hours, minutes;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);

        if (!Utils.hasConnection(this)) {
            Toast.makeText(LoggedActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }

        session = new SessionManager(this);
        ID = getIntent().getStringExtra("id");
        email = getIntent().getStringExtra("email");

        setUI();
        getEvent();

        lineDown.setVisibility(View.GONE);
        compactCalendarView.closeCalendar();

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                txtCalendar.setText(getString(R.string.calendar) + " ( " + dateFormatForMonth.format(compactCalendarView.getSelectedDayOfCurrentMonth()) + " )");
                List<Event> bookingsFromMap = compactCalendarView.getEvents(dateClicked);
                if(bookingsFromMap != null){
                    events.clear();
                    for(Event booking : bookingsFromMap){
                        event = new EventObject();
                        event.setmDate(booking.getEvent().getmDate());
                        event.setmStart(booking.getEvent().getmStart());
                        event.setmTag(booking.getEvent().getmTag());
                        event.setmContent(booking.getEvent().getmContent());
                        event.setmColor(booking.getEvent().getmColor());
                        events.add(event);
                    }
                    adapter = new ListEventsAdapter(LoggedActivity.this, events);
                    listEvents.setAdapter(adapter);
                }
            }

            @Override
            public void onMonthScrollPre(Date firstDayOfNewMonth) {
                showCurrentMonth("pre");
            }

            @Override
            public void onMonthScrollNext(Date firstDayOfNewMonth) {
                showCurrentMonth("next");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionLogOut:
                logoutUser();
                break;

        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.slideDown:
                if(checkArrow) {
                    imageArrow.setImageResource(R.drawable.ic_arrow_up);
                    imageArrow.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate180_down));
                    compactCalendarView.hideCalendarFast(lineUp, lineDown);
                } else {
                    imageArrow.setImageResource(R.drawable.ic_arrow_down);
                    imageArrow.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate180_up));
                    compactCalendarView.showCalendarFast();
                    lineDown.setVisibility(View.VISIBLE);
                    lineUp.setVisibility(View.GONE);
                }
                checkArrow = !checkArrow;
                break;

            case R.id.fab:
                Intent intent = new Intent(this, ScheduleActivity.class);
                intent.putExtra("color", currentColor);
                intent.putExtra("selectedDay", dateFormatForMonth.format(compactCalendarView.getSelectedDayOfCurrentMonth()));
                startActivityForResult(intent, Values.REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Values.REQUEST_CODE && resultCode == Values.RESULT_CODE) {
            Bundle bundle = data.getBundleExtra("schedule");
            int year = bundle.getInt("year");
            int month = bundle.getInt("month");
            int day = bundle.getInt("day");
            int hours = bundle.getInt("hour");
            int minutes = bundle.getInt("minute");
            String tags = bundle.getString("tags");
            String content = bundle.getString("content");
            int colorEvent = bundle.getInt("color");

            event = new EventObject();
            event.setmDate(ConvertValues.convertDate(year, month, day));
            event.setmStart(ConvertValues.convertTime(hours, minutes));
            event.setmTag(tags);
            event.setmContent(content);
            event.setmColor(colorEvent);

            // create events server
            createEvent(ID, ConvertValues.convertDate(year, month, day), ConvertValues.convertTime(hours, minutes), tags, content, String.valueOf(colorEvent));

            compactCalendarView.addEvent(setEvents(getTimeEvents(year, month, day, hours, minutes), event), false);
            Log.e("fuck", String.valueOf(getTimeEvents(year, month, day, hours, minutes)));
            compactCalendarView.invalidate();
        }
    }

    // set email session
    @Override
    public void onBackPressed() {
        if (session.isLoggedIn()) {
            super.onBackPressed();
        } else {
            logoutUser();
        }
    }

    private Event setEvents(long time, EventObject event) {
        return new Event(ContextCompat.getColor(this, color[19]), time, event);
    }

    private void createEvent(final String id, final String date, final String time, final String tag, final String content, final String color) {

        ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), true, progressDialog);

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_EVENT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), false, progressDialog);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");
                    if (error) {
                        String errorMsg = jsonObject.getString("error_msg");
                        Toast.makeText(LoggedActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // server do not return value
                    Toast.makeText(LoggedActivity.this, "" + e.toString().substring(24), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoggedActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), true, progressDialog);
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("date", date);
                params.put("time", time);
                params.put("tag", tag);
                params.put("content", content);
                params.put("color", color);

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void getEvent() {

        ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), true, progressDialog);

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_GET_EVENT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), false, progressDialog);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");
                    if (!error) {
                        JSONArray jsonArray = jsonObject.getJSONArray("event");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectChild = jsonArray.getJSONObject(i);
                            event = new EventObject();
                            event.setmDate(jsonObjectChild.getString("date"));
                            event.setmStart(jsonObjectChild.getString("time"));
                            event.setmTag(jsonObjectChild.getString("tag"));
                            event.setmContent(jsonObjectChild.getString("content"));
                            event.setmColor(Integer.parseInt(jsonObjectChild.getString("color")));

                            getDate(jsonObjectChild.getString("date"), jsonObjectChild.getString("time"));
                            compactCalendarView.addEvent(setEvents(getTimeEvents(year, monthEvents, day, hours, minutes), event), false);
                            compactCalendarView.invalidate();
                        }
                    } else {
                        Toast.makeText(LoggedActivity.this, "fuck", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // server do not return value
                    Toast.makeText(LoggedActivity.this, "" + e.toString().substring(24), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoggedActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_event), false, progressDialog);
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", ID);

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    private long getTimeEvents(int year, int month, int day, int hours, int minutes) {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    private void getDate(String currentDay, String time) {
        day = Integer.parseInt(currentDay.substring(0, 2));
        monthEvents = Values.getMonth(currentDay.substring(5, 8));
        year = Integer.parseInt(currentDay.substring(11));

        hours = Integer.parseInt(time.substring(0,2));
        minutes = Integer.parseInt(time.substring(3));
    }

    // ui {
    private void setUI() {
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_custom_text);
        ((TextViewCustom) getSupportActionBar().getCustomView().findViewById(R.id.helloUser)).setText(email);

        getId();
        setColor();
        setGuiColor();
        compactCalendarView.setLayout(btnArrow, elevation);
    }

    // user
    private void logoutUser() {
        session.setLogin(false, email, "");
        Intent intent = new Intent(LoggedActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    // onCreate component
    private void getId() {
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        txtCalendar = (TextViewCustom) findViewById(R.id.textCalendar);
        btnArrow = (RelativeLayout) findViewById(R.id.slideDown);
        imageArrow = (ImageView) findViewById(R.id.imageArrow);
        lineDown = findViewById(R.id.lineDown);
        lineUp = findViewById(R.id.lineUp);
        elevation = (ImageView) findViewById(R.id.elevation);
        btnArrow.setOnClickListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        listEvents = (ListView) findViewById(R.id.listEvents);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        events = new ArrayList<>();
        adapter = new ListEventsAdapter(this, events);
        listEvents.setAdapter(adapter);
    }

    private void setGuiColor() {
        month = monthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());
        setColorBackGround(month);
        txtCalendar.setText(getString(R.string.calendar) + " ( " + dateFormatForMonth.format(compactCalendarView.getCurrentDayOfCurrentMonth()) + " )");
    }

    private void setColor() {
        color = new int[24];
        color[0] = R.color.jan;
        color[1] = R.color.feb;
        color[2] = R.color.mar;
        color[3] = R.color.apr;
        color[4] = R.color.may;
        color[5] = R.color.jun;
        color[6] = R.color.jul;
        color[7] = R.color.aug;
        color[8] = R.color.sep;
        color[9] = R.color.oct;
        color[10] = R.color.nov;
        color[11] = R.color.dec;
        color[12] = R.color.jan_light;
        color[13] = R.color.feb_light;
        color[14] = R.color.mar_light;
        color[15] = R.color.apr_light;
        color[16] = R.color.may_light;
        color[17] = R.color.jun_light;
        color[18] = R.color.jul_light;
        color[19] = R.color.aug_light;
        color[20] = R.color.sep_light;
        color[21] = R.color.oct_light;
        color[22] = R.color.nov_light;
        color[23] = R.color.dec_light;
    }

    private void chageColor(int colorPri, int colorLight) {
        btnArrow.setBackgroundColor(ContextCompat.getColor(this, colorPri));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, colorPri)));
        elevation.setBackgroundColor(ContextCompat.getColor(this, colorPri));
        compactCalendarView.setCurrentDayBackgroundColor(ContextCompat.getColor(this, colorPri));
        compactCalendarView.setCurrentSelectedDayBackgroundColor(ContextCompat.getColor(this, colorLight));
        fab.setBackgroundTintList(new ColorStateList(new int[][]{new int[]{0}}, new int[]{ContextCompat.getColor(this, colorPri)}));
        currentColor = colorPri;
    }

    private void setColorBackGround(String month) {
        switch (month) {
            case "Jan":
                chageColor(color[0], color[12]);
                break;

            case "Feb":
                chageColor(color[1], color[13]);
                break;

            case "Mar":
                chageColor(color[2], color[14]);
                break;

            case "Apr":
                chageColor(color[3], color[15]);
                break;

            case "May":
                chageColor(color[4], color[16]);
                break;

            case "Jun":
                chageColor(color[5], color[17]);
                break;

            case "Jul":
                chageColor(color[6], color[18]);
                break;

            case "Aug":
                chageColor(color[7], color[19]);
                break;

            case "Sep":
                chageColor(color[8], color[20]);
                break;

            case "Oct":
                chageColor(color[9], color[21]);
                break;

            case "Nov":
                chageColor(color[10], color[22]);
                break;

            case "Dec":
                chageColor(color[11], color[23]);
                break;
        }
    }

    // set Motion color
    private void setMotionColorStatus(String status, int... color) {
        if(status.compareTo("next") == 0) {
            compactCalendarView.setMotionColor(getSupportActionBar(), elevation, btnArrow, fab, color[0], color[1], color[2]);
        } else if(status.compareTo("pre") == 0) {
            compactCalendarView.setMotionColor(getSupportActionBar(), elevation, btnArrow, fab, color[3], color[1], color[2]);
        }
        currentColor = color[1];
    }

    private void motionChangeColor(String month, String status) {
        switch (month) {
            case "Jan":
                setMotionColorStatus(status, color[11], color[0], color[12], color[1]);
                break;

            case "Feb":
                setMotionColorStatus(status, color[0], color[1], color[13], color[2]);
                break;

            case "Mar":
                setMotionColorStatus(status, color[1], color[2], color[14], color[3]);
                break;

            case "Apr":
                setMotionColorStatus(status, color[2], color[3], color[15], color[4]);
                break;

            case "May":
                setMotionColorStatus(status, color[3], color[4], color[16], color[5]);
                break;

            case "Jun":
                setMotionColorStatus(status, color[4], color[5], color[17], color[6]);
                break;

            case "Jul":
                setMotionColorStatus(status, color[5], color[6], color[18], color[7]);
                break;

            case "Aug":
                setMotionColorStatus(status, color[6], color[7], color[19], color[8]);
                break;

            case "Sep":
                setMotionColorStatus(status, color[7], color[8], color[20], color[9]);
                break;

            case "Oct":
                setMotionColorStatus(status, color[8], color[9], color[21], color[10]);
                break;

            case "Nov":
                setMotionColorStatus(status, color[9], color[10], color[22], color[11]);
                break;

            case "Dec":
                setMotionColorStatus(status, color[10], color[11], color[23], color[0]);
                break;
        }
    }

    private void showCurrentMonth(String status) {
        month = monthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());
        motionChangeColor(month, status);
        if(Values.getMOnth(Calendar.getInstance().get(Calendar.MONTH) + 1).compareTo(monthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth())) == 0) {
            txtCalendar.setText(getString(R.string.calendar) + " ( " + dateFormatForMonth.format(compactCalendarView.getCurrentDayOfCurrentMonth()) + " )");
        } else {
            txtCalendar.setText(getString(R.string.calendar) + " ( " + dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()) + " )");
        }
    }
    // }
}
