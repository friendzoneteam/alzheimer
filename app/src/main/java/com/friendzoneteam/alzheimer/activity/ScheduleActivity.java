package com.friendzoneteam.alzheimer.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.adapter.ListColorsAdapter;
import com.friendzoneteam.alzheimer.adapter.ListTagsAdapter;
import com.friendzoneteam.alzheimer.entity.ColorsObject;
import com.friendzoneteam.alzheimer.entity.TagsObject;
import com.friendzoneteam.alzheimer.utils.ConvertValues;
import com.friendzoneteam.alzheimer.utils.Values;
import com.mbcorp.guidesignlib.ProgressDialogType;
import com.mbcorp.json.app.AppConfig;
import com.mbcorp.json.app.AppController;
import com.mbcorp.textfontslib.EditTextCustom;
import com.mbcorp.textfontslib.TextViewCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScheduleActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout toolbar;
    private ImageView btnNo;
    private ImageView btnYes;
    private TextViewCustom btnBegin;
    private TextViewCustom btnStart;
    private TextViewCustom btnTags;
    private EditTextCustom editContent;
    private TextViewCustom btnColor;
    private ImageView imageColor;

    private DialogFragment datePicker;
    private int year, month, day;

    private DialogFragment timePicker;
    private int hours = 0, minutes = 0;

    private String tagsName;
    private String content;
    private int colorEvent;

    private int color;
    private String currentDay;
    private Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        color = getIntent().getIntExtra("color", 0);
        currentDay = getIntent().getStringExtra("selectedDay");

        getId();
        onCreate();
    }

    private void getId() {
        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        btnNo = (ImageView) findViewById(R.id.actionNo);
        btnYes = (ImageView) findViewById(R.id.actionYes);
        btnBegin = (TextViewCustom) findViewById(R.id.begin);
        btnStart = (TextViewCustom) findViewById(R.id.start);
        btnTags = (TextViewCustom) findViewById(R.id.tags);
        editContent = (EditTextCustom) findViewById(R.id.comment);
        btnColor = (TextViewCustom) findViewById(R.id.color);
        imageColor = (ImageView) findViewById(R.id.imageColor);
        btnNo.setOnClickListener(this);
        btnYes.setOnClickListener(this);
        btnBegin.setOnClickListener(this);
        btnStart.setOnClickListener(this);
        btnTags.setOnClickListener(this);
        btnColor.setOnClickListener(this);
    }

    private void onCreate() {
        window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, color));
        }

        toolbar.setBackgroundColor(ContextCompat.getColor(this, color));
        btnBegin.setText(currentDay);

        tagsName = getString(R.string.tags);
        colorEvent = R.drawable.image_round_blue_light;
        getDate();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.actionNo:
                finish();
                break;

            case R.id.actionYes:
                if(tagsName.compareTo(getString(R.string.tags)) != 0) {
                    Intent intent = getIntent();
                    Bundle bundle = new Bundle();
                    bundle.putInt("year", year);
                    bundle.putInt("month", month);
                    bundle.putInt("day", day);
                    bundle.putInt("hour", hours);
                    bundle.putInt("minute", minutes);
                    bundle.putString("tags", tagsName);
                    content = String.valueOf(editContent.getText());
                    String contentEvent;
                    if(content.compareTo("") == 0) {
                        contentEvent = getString(R.string.no_content);
                    } else {
                        contentEvent = content;
                    }
                    bundle.putString("content", contentEvent);
                    bundle.putInt("color", colorEvent);
                    intent.putExtra("schedule", bundle);
                    setResult(Values.RESULT_CODE, intent);
                    finish();
                } else {
                    Toast.makeText(ScheduleActivity.this, getString(R.string.infor_details), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.begin:
                datePicker = new DatePicker();
                datePicker.show(getSupportFragmentManager(), "DatePicker");
                break;

            case R.id.start:
                timePicker = new TimePicker();
                timePicker.show(getSupportFragmentManager(), "TimePicker");
                break;

            case R.id.tags:
                createTagsDialog();
                break;

            case R.id.color:
                createColorsDialog();
                break;

        }
    }

    private void getDate() {
        day = Integer.parseInt(currentDay.substring(0, 2));
        month = Values.getMonth(currentDay.substring(5, 8));
        year = Integer.parseInt(currentDay.substring(11));
    }

    @SuppressLint("ValidFragment")
    class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new DatePickerDialog(getActivity(), this, year, month - 1, day);
        }

        @Override
        public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            ScheduleActivity.this.year = year; month = monthOfYear + 1; day = dayOfMonth;
            currentDay = ConvertValues.convertDate(year, month, day);
            btnBegin.setText(currentDay);
        }
    }

    @SuppressLint("ValidFragment")
    class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new TimePickerDialog(getActivity(), this, hours, minutes, true);
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
            hours = hourOfDay; minutes = minute;
            btnStart.setText(ConvertValues.convertTime(hours, minutes));
        }
    }

    private void createTagsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog dialog = builder.create();

        //(LAYOUT)
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.list_tags, null);

        final ArrayList<TagsObject> tags = Values.getTags();
        ListView listTags = (ListView) dialogLayout.findViewById(R.id.listTags);
        ListTagsAdapter adapter = new ListTagsAdapter(this, tags);
        listTags.setAdapter(adapter);
        listTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tagsName = tags.get(position).getmTagsName();
                btnTags.setText(tagsName);
                dialog.dismiss();
            }
        });

        dialog.setView(dialogLayout);
        dialog.setTitle(getString(R.string.tags));
        dialog.show();
    }

    private void createColorsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog dialog = builder.create();

        //(LAYOUT)
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.list_tags, null);

        final ArrayList<ColorsObject> colors = Values.getColors();
        ListView listTags = (ListView) dialogLayout.findViewById(R.id.listTags);
        ListColorsAdapter adapter = new ListColorsAdapter(this, colors);
        listTags.setAdapter(adapter);
        listTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                btnColor.setText(colors.get(position).getColorName());
                colorEvent = colors.get(position).getColor();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageColor.setBackground(ContextCompat.getDrawable(ScheduleActivity.this, colors.get(position).getColor()));
                } else {
                    imageColor.setBackgroundDrawable(ContextCompat.getDrawable(ScheduleActivity.this, colors.get(position).getColor()));
                }
                dialog.dismiss();
            }
        });

        dialog.setView(dialogLayout);
        dialog.setTitle(getString(R.string.color_title));
        dialog.show();
    }
}
