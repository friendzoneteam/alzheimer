package com.friendzoneteam.alzheimer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.friendzoneteam.alzheimer.R;
import com.friendzoneteam.alzheimer.utils.Utils;
import com.friendzoneteam.alzheimer.utils.Values;
import com.mbcorp.guidesignlib.ProgressDialogType;
import com.mbcorp.json.app.AppConfig;
import com.mbcorp.json.app.AppController;
import com.mbcorp.textfontslib.EditTextCustom;
import com.mbcorp.textfontslib.TextViewCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditTextCustom editEmail;
    private EditTextCustom editPassword;
    private EditTextCustom editConfirmPassword;
    private TextViewCustom btnRegister;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (!Utils.hasConnection(this)) {
            Toast.makeText(RegisterActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        editEmail = (EditTextCustom) findViewById(R.id.email);
        editPassword = (EditTextCustom) findViewById(R.id.password);
        editConfirmPassword = (EditTextCustom) findViewById(R.id.confirmPassword);
        btnRegister = (TextViewCustom) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    // check register
    private void registerUser(final String email, final String password) {

        ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_register), true, progressDialog);

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_register), false, progressDialog);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");
                    if (!error) {
                        Toast.makeText(RegisterActivity.this, getString(R.string.login_success), Toast.LENGTH_LONG).show();
                        Intent intent = getIntent();
                        intent.putExtra("email", email);
                        setResult(Values.RESULT_CODE, intent);
                        finish();
                    } else {
                        String errorMsg = jsonObject.getString("error_msg");
                        Toast.makeText(RegisterActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                ProgressDialogType.progressDialogNormal(getString(R.string.progress_loading_register), true, progressDialog);
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                JSONObject json = new JSONObject();
                try {
                    json.put("email", email);
                    json.put("password", password);
                } catch (JSONException e) {
                    Toast.makeText(RegisterActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }

                // encode json with base64
                try {
                    byte[] data = String.valueOf(json).getBytes("UTF-8");
                    params.put("json", Base64.encodeToString(data, Base64.DEFAULT));
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(RegisterActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq);
    }
    // ====

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                if (!Utils.hasConnection(this)) {
                    Toast.makeText(RegisterActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
                } else {
                    String email = editEmail.getText().toString().trim();
                    String password = editPassword.getText().toString().trim();
                    String confirmPassword = editConfirmPassword.getText().toString().trim();

                    if (!Utils.isValidEmail(email)) {
                        Toast.makeText(RegisterActivity.this, getString(R.string.gmail_validation), Toast.LENGTH_SHORT).show();
                    } else {
                        if (!email.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty()) {
                            if (password.equals(confirmPassword)) {
                                registerUser(email, password);
                            } else {
                                Toast.makeText(RegisterActivity.this, getString(R.string.register_difference_confirm_password), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(RegisterActivity.this, getString(R.string.register_infor), Toast.LENGTH_LONG).show();
                        }
                    }
                }
                break;
        }
    }
}