package com.friendzoneteam.alzheimer.entity;

/**
 * Created by MINH-PC on 8/19/2016.
 */
public class ColorsObject {
    private String colorName;
    private int color;

    public String getColorName() {
        return colorName;
    }

    public int getColor() {
        return color;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
