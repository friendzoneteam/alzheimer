package com.friendzoneteam.alzheimer.entity;

/**
 * Created by Minh on 7/29/2016.
 */
public class EventObject {
    private String mDate;
    private String mStart;
    private String mTag;
    private String mContent;
    private int mColor;

    public String getmDate() {
        return mDate;
    }

    public String getmStart() {
        return mStart;
    }

    public String getmTag() {
        return mTag;
    }

    public String getmContent() {
        return mContent;
    }

    public int getmColor() {
        return mColor;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public void setmStart(String mStart) {
        this.mStart = mStart;
    }

    public void setmTag(String mTag) {
        this.mTag = mTag;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }

    public void setmColor(int mColor) {
        this.mColor = mColor;
    }
}
