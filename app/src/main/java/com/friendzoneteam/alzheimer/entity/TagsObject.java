package com.friendzoneteam.alzheimer.entity;

/**
 * Created by Minh on 7/25/2016.
 */
public class TagsObject {
    private String mTagsName;
    private int mTagsIcon;

    public String getmTagsName() {
        return mTagsName;
    }

    public int getmTagsIcon() {
        return mTagsIcon;
    }

    public void setmTagsName(String mTagsName) {
        this.mTagsName = mTagsName;
    }

    public void setmTagsIcon(int mTagsIcon) {
        this.mTagsIcon = mTagsIcon;
    }
}
